package br.com.oi.topApplication.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ricardo
 */

@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true)
@Table(name = "Settings", uniqueConstraints = {@UniqueConstraint(columnNames = "ID")})
public class Setting {
    @Id
    @GeneratedValue
    private int id;
    private String Language;
    private Star Star;
    private int Forks;
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the Language
     */
    public String getLanguage() {
        return Language;
    }
    /**
     * @param Language the Language to set
     */
    public void setLanguage(String Language) {
        this.Language = Language;
    }

    /**
     * @return the Stars
     */
    public Star getStar() {
        return Star;
    }

    /**
     * @param Stars the Stars to set
     */
    public void setStar(Star Star) {
        this.Star = Star;
    }

    /**
     * @return the Forks
     */
    public int getForks() {
        return Forks;
    }

    /**
     * @param Forks the Forks to set
     */
    public void setForks(int Forks) {
        this.Forks = Forks;
    }
}


