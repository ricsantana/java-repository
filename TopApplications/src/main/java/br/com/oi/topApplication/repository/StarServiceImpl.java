/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.repository;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import br.com.oi.topApplication.entity.Star;
import java.util.List;

/**
 *
 * @author ricardo
 */
public class StarServiceImpl implements StarService {

    @Resource
    private StarRepository starRepository;

    @Override
    @Transactional
    public Star create(Star star) {
        Star createdstar = star;
        return starRepository.save(createdstar);
    }

    @Override
    @Transactional
    public Star findById(int id) {
        return starRepository.findOne(id);
    }

    @Override
    @Transactional()
    public Star delete(int id) throws NotFoundException {
        Star deletedStar = starRepository.findOne(id);

        if (deletedStar == null) {
            throw new NotFoundException();
        }

        starRepository.delete(deletedStar);
        return deletedStar;
    }

    @Override
    @Transactional
    public List<Star> findAll() {
        return starRepository.findAll();
    }

    @Override
    @Transactional
    public Star update(Star star) throws NotFoundException {
        Star updatedStar = starRepository.findOne(star.getId());

        if (updatedStar == null) {
            throw new NotFoundException();
        }
        
        updatedStar.setMin(star.getMin());
        updatedStar.setMax(star.getMax());
        
        return updatedStar;
    }

}
