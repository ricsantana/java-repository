package br.com.oi.topApplication.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 *
 * @author ricardo
 */
@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true)
@Table(name= "SettingTypes", uniqueConstraints={@UniqueConstraint(columnNames = "ID")})
public class SettingTypes implements Serializable{
 
    @Id
    @Column(name = "Id", unique = true, nullable = false)
    private int id;
    @Column(name = "Description", unique = true, nullable = false, length = 100)
    private String Description;
    @Column(name = "CreatedAt", unique = false, nullable = false)
    private Date CreatedAt;
    @Column(name = "UpdatedAt", unique = false, nullable = false)
    private Date UpdatedAt;

    
    public SettingTypes(int id, String description, Date createdAt, Date updatedAt ){
        this.id =id;
        this.Description = description;
        this.CreatedAt = createdAt;
        this.UpdatedAt = updatedAt;
    }
    
    private static final long serialVersionUID = -1798070786993154676L; 
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param Description the Description to set
     */
    public void setDescription(String Description) {
        this.Description = Description;
    }

    /**
     * @return the CreatedAt
     */
    public Date getCreatedAt() {
        return CreatedAt;
    }

    /**
     * @param CreatedAt the CreatedAt to set
     */
    public void setCreatedAt(Date CreatedAt) {
        this.CreatedAt = CreatedAt;
    }

    /**
     * @return the UpdatedAt
     */
    public Date getUpdatedAt() {
        return UpdatedAt;
    }

    /**
     * @param UpdatedAt the UpdatedAt to set
     */
    public void setUpdatedAt(Date UpdatedAt) {
        this.UpdatedAt = UpdatedAt;
    }
}
