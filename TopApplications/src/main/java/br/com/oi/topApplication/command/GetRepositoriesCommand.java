package br.com.oi.topApplication.command;

import br.com.oi.topApplication.convert.JsonToItem;
import br.com.oi.topApplication.convert.JsonToOwner;
import br.com.oi.topApplication.convert.JsonToRepository;
import br.com.oi.topApplication.entity.Item;
import br.com.oi.topApplication.entity.Owner;
import br.com.oi.topApplication.entity.Repository;
import br.com.oi.topApplication.entity.Setting;
import br.com.oi.topApplication.util.WebClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author ricardo
 */
public class GetRepositoriesCommand extends Configuration implements Command<Repository> {

    private static final String URL_RESTFULL = "https://api.github.com/search/repositories";

    public GetRepositoriesCommand(Setting setting) {
        super(setting);
    }

    @Override
    public Repository handler() {

        WebClient webClient = new WebClient();
        
        Repository repository = null;

        InputStream inputstream = webClient.Get(URL_RESTFULL + super.toString());

        try {

            BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(inputstream)));

            JSONParser parser = new JSONParser();

            JSONObject json = (JSONObject) parser.parse(bufferedReader);

            repository = JSONRepository(json);

        } catch (IOException ex) {
            Logger.getLogger(GetRepositoriesCommand.class.getName()).log(Level.SEVERE, null, ex);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return repository;
    }

    private Repository JSONRepository(JSONObject json) {

        Repository repository = JsonToRepository.Convert(json);
        
        JSONArray itemsJSONArray = (JSONArray) json.get("items");

        List<Item> items = new ArrayList<Item>();

        for (int i = 0; i < itemsJSONArray.size(); i++) {
            
            JSONObject jsonItem = (JSONObject) itemsJSONArray.get(i);
           
            Item item = JsonToItem.Convert(jsonItem);            
           
            JSONObject jsonOwner = (JSONObject) jsonItem.get("owner");

            Owner owner = JsonToOwner.Convert(jsonOwner);
            
            item.setOwner(owner);
            
            items.add(item);

        }
        repository.setItems(items);

        return repository;
    }
}
