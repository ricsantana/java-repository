/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.convert;

import br.com.oi.topApplication.entity.Owner;
import org.json.simple.JSONObject;

/**
 *
 * @author ricardo
 */
public class JsonToOwner {

    public static Owner Convert(JSONObject jsonOwner) {
        
        Owner owner = new Owner();
        
        owner.setId((long) jsonOwner.get("id"));
        owner.setAvatar((String) jsonOwner.get("avatar_url"));
        owner.setName((String) jsonOwner.get("login"));
        owner.setLogin((String) jsonOwner.get("login"));
        owner.setProfile((String) jsonOwner.get("url"));
        owner.setRepository((String) jsonOwner.get("repos_url"));

        return owner;
    }
}
