package br.com.oi.topApplication.command;

import br.com.oi.topApplication.convert.JsonToOwner;
import br.com.oi.topApplication.entity.Owner;
import br.com.oi.topApplication.entity.Setting;
import br.com.oi.topApplication.util.WebClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author ricardo
 */
public class GetOnwersCommand extends Configuration implements Command<List<Owner>> {

    private static final String URL_RESTFULL = "https://api.github.com/search/users?q=java";

    public GetOnwersCommand(Setting setting) {
        super(setting);
    }

    @Override
    public List<Owner> handler() {

        List<Owner> owners = null;

        try {
            
            WebClient webClient = new WebClient();

            InputStream inputstream = webClient.Get(URL_RESTFULL); //+ super.toString());    

            BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(inputstream)));

            JSONParser parser = new JSONParser();

            JSONObject json = (JSONObject) parser.parse(bufferedReader);

            owners = JSONOwners(json);

        } catch (IOException ex) {
            Logger.getLogger(GetOnwersCommand.class.getName()).log(Level.SEVERE, null, ex);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return owners;

    }

    private List<Owner> JSONOwners(JSONObject json) {

        List<Owner> owners = new ArrayList<Owner>();

        JSONArray OwnersJSONArray = (JSONArray) json.get("items");

        for (int i = 0; i < OwnersJSONArray.size(); i++) {

            JSONObject jsonOwner = (JSONObject) OwnersJSONArray.get(i);

            Owner owner = JsonToOwner.Convert(jsonOwner);

            owners.add(owner);
        }

        return owners;
    }

}
