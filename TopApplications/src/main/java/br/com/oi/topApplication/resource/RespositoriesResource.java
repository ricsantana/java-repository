
package br.com.oi.topApplication.resource;
import br.com.oi.topApplication.command.GetRepositoriesCommand;
import br.com.oi.topApplication.entity.Repository;
import br.com.oi.topApplication.entity.Setting;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
 
 

/**
 * REST Web Service
 *
 * @author ricardo
 */
@Path("Respositories")
public class RespositoriesResource {

    @Context
    private UriInfo context;

    public RespositoriesResource() {
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRepository() {
        
       Repository  repository = new Repository();
        ObjectMapper mapper = new ObjectMapper();
        try {

            GetRepositoriesCommand command = new GetRepositoriesCommand(new Setting());
            repository =   command.handler();

            if (repository == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("Nenhum repositório encontrado").build();
            }
            //mapper.writeValueAsString(mapper)
         return Response.ok( mapper.writeValueAsString(repository.getItems()), MediaType.APPLICATION_JSON).build();
         
        } catch (Exception e) {
            return Response.status(Response.Status.EXPECTATION_FAILED).entity("Unexpected error").build();
        }

          
    }

}
