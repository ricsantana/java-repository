package br.com.oi.topApplication.resource;

import br.com.oi.topApplication.command.GetOnwersCommand;
import br.com.oi.topApplication.entity.Owner;
import br.com.oi.topApplication.entity.Setting;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author ricardo
 */
@Path("Owner")
public class OwnerResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of OwnerResource
     */
    public OwnerResource() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOwner() {

        List< Owner> owners = new ArrayList<Owner>();
        ObjectMapper mapper = new ObjectMapper();
        try {

            GetOnwersCommand command = new GetOnwersCommand(new Setting());
            owners = (ArrayList<Owner>) command.handler();

            if (owners == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("Nenhum owner encontrado").build();
            }
            
           return Response.ok( mapper.writeValueAsString(owners), MediaType.APPLICATION_JSON).build();
            
        } catch (Exception e) {
             return Response.status(Response.Status.EXPECTATION_FAILED).entity("Unexpected error").build();
        }
    }

}
