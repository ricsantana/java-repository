package br.com.oi.topApplication.command;

import br.com.oi.topApplication.entity.Setting;

/**
 *
 * @author ricardo
 */
public abstract class Configuration {
  private Setting setting;

    public Configuration( Setting settings) {
      this.setting=setting;
    }
    
     /**
     * @return the settings
     */
    public Setting getSettings() {
        return setting;
    }

    /**
     * @param settings the settings to set
     */
    public void setSettings(Setting setting) {
        this.setting = setting;
    } 
    
    @Override
    public String toString(){
        return "?q=tetris+language:assembly&sort=stars&order=desc";
    }
}
