/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.convert;
import br.com.oi.topApplication.entity.Repository;
import org.json.simple.JSONObject;
/**
 *
 * @author ricardo
 */
public class JsonToRepository {
   
       public static Repository Convert(JSONObject jsonRepository) {

        Repository repository = new Repository();

        repository.setTotal_count((long) jsonRepository.get("total_count"));
        repository.setIncomplete_results((boolean) jsonRepository.get("incomplete_results"));       

        return repository;
    }
}
