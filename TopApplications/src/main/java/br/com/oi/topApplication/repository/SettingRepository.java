/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.oi.topApplication.entity.Setting;
/**
 *
 * @author ricardo
 */
public interface SettingRepository extends JpaRepository<Setting, Integer>{
    
}
