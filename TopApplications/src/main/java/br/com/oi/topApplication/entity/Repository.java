package br.com.oi.topApplication.entity;

 
import java.util.List;

/**
 *
 * @author ricardo
 */
public class Repository {

    private long total_count;
    private boolean incomplete_results;
    private List<Item> items;

    public long getTotal_count() {
        return total_count;
    }

    public void setTotal_count(long total_count) {
        this.total_count = total_count;
    }

    public boolean getIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}
