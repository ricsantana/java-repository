/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.repository;
import br.com.oi.topApplication.entity.Star;
import java.util.List;
/**
 *
 * @author ricardo
 */
public interface StarService {
  
    public Star create(Star star);
    public Star delete(int id) ;
    public List<Star> findAll();
    public Star update(Star star) ;
    public Star findById(int id); 
}
