package br.com.oi.topApplication.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * WebClient class responsible for integrating web services
 *
 * @author ricardo
 */
public class WebClient {

    public InputStream Get(String endPoint) {
        try {

            HttpURLConnection connection = null;
            connection  =(HttpURLConnection) new URL(endPoint).openConnection(); 
                InputStream content = connection.getInputStream();
            

            return content;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {

        }
    }
}
