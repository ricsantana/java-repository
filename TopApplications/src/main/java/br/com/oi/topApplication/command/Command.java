 package br.com.oi.topApplication.command;
/**
 *
 * @author ricardo
 */
public interface Command<T> {

    T handler();
}
