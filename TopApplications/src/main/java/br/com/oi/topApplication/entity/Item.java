/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.entity;

import java.util.Date;

/**
 *
 * @author ricardo
 */
public class Item {

    public Item() {
    }

    private long id;
    private String name;
    private String fullName;
    private String descriptions;
    private String url;
    private long stars;
    private long forks;
    private long openIssues;
    private Owner owner;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the descriptions
     */
    public String getDescriptions() {
        return descriptions;
    }

    /**
     * @param descriptions the descriptions to set
     */
    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the stars
     */
    public long getStars() {
        return stars;
    }

    /**
     * @param stars the stars to set
     */
    public void setStars(long stars) {
        this.stars = stars;
    }

    /**
     * @return the forks
     */
    public long getForks() {
        return forks;
    }

    /**
     * @param forks the forks to set
     */
    public void setForks(long forks) {
        this.forks = forks;
    }

    /**
     * @return the openIssues
     */
    public long getOpenIssues() {
        return openIssues;
    }

    /**
     * @param openIssues the openIssues to set
     */
    public void setOpenIssues(long openIssues) {
        this.openIssues = openIssues;
    }

    /**
     * @return the owner
     */
    public Owner getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Owner owner) {
        this.owner = owner;
    }

}
