/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.repository;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import br.com.oi.topApplication.entity.Setting;
import java.util.List;

/**
 *
 * @author ricardo
 */
@Service
public class SettingServiceImpl implements SettingService {

    @Resource
    private SettingRepository settingRepository;

    @Override
    @Transactional
    public Setting create(Setting setting) {
        Setting createdSetting = setting;
        return settingRepository.save(createdSetting);
    }

    @Override
    @Transactional
    public Setting findById(int id) {
        return settingRepository.findOne(id);
    }

    @Override
    @Transactional()
    public Setting delete(int id) throws NotFoundException {
        Setting deletedSetting = settingRepository.findOne(id);

        if (deletedSetting == null) {
            throw new NotFoundException();
        }

        settingRepository.delete(deletedSetting);
        return deletedSetting;
    }

    @Override
    @Transactional
    public List<Setting> findAll() {
        return settingRepository.findAll();
    }

    @Override
    @Transactional
    public Setting update(Setting setting) throws NotFoundException {
        Setting updatedSetting = settingRepository.findOne(setting.getId());

        if (updatedSetting == null) {
            throw new NotFoundException();
        }

        updatedSetting.setForks(setting.getForks());
        updatedSetting.setLanguage(setting.getLanguage());
        updatedSetting.setStar(setting.getStar());

        return updatedSetting;
    }
}
