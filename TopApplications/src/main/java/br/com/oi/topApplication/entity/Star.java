package br.com.oi.topApplication.entity;
 
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ricardo
 */
@Entity 

@Table(name = "Stars")
 
public class Star  implements Serializable {
     @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int mine;
    private int maxi;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the min
     */
    public int getMin() {
        return mine;
    }

    /**
     * @param min the min to set
     */
    public void setMin(int min) {
        this.mine = min;
    }

    /**
     * @return the max
     */
    public int getMax() {
        return maxi;
    }

    /**
     * @param max the max to set
     */
    public void setMax(int max) {
        this.maxi = max;
    }
}
