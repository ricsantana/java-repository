/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.convert;

import br.com.oi.topApplication.entity.Item;

import org.json.simple.JSONObject;

/**
 *
 * @author ricardo
 */
public class JsonToItem {

    public static Item Convert(JSONObject jsonItem) {

        Item item = new Item();

        item.setId((long) jsonItem.get("id"));
        item.setName((String) jsonItem.get("name"));
        item.setFullName((String) jsonItem.get("full_name"));
        item.setDescriptions((String) jsonItem.get("description"));
        item.setUrl((String) jsonItem.get("url"));
        item.setStars((long) jsonItem.get("stargazers_count"));
        item.setForks((long) jsonItem.get("forks"));
        item.setOpenIssues((long) jsonItem.get("open_issues"));

        return item;
    }
}
