package br.com.oi.topApplication.resource;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
 import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
/**
 * REST Web Service
 *
 * @author ricardo
 */
@Path("rest")
public class TopApplicationResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TopApplicationResource
     */
    public TopApplicationResource() {
    }

    /**
     * Retrieves representation of an instance of br.com.oi.topApplication.TopApplicationResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson() {
        //TODO return proper representation object
       /// throw new UnsupportedOperationException();
       
       String output = "<h1>Hello World!<h1>" +
				"<p>RESTful Service is running ... <br>Ping @ " + new Date().toString() + "</p<br>";
		return Response.status(200).entity(output).build();
	}
    }

  

