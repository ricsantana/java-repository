/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.oi.topApplication.repository;

import br.com.oi.topApplication.entity.Setting;
import java.util.List;

/**
 *
 * @author ricardo
 */
public interface SettingService {
    
    public Setting create(Setting setting);
    public Setting delete(int id) ;
    public List<Setting> findAll();
    public Setting update(Setting setting) ;
    public Setting findById(int id); 
}
