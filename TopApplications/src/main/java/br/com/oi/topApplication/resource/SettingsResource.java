package br.com.oi.topApplication.resource;

import br.com.oi.topApplication.entity.Setting;
import br.com.oi.topApplication.repository.SettingService;
import br.com.oi.topApplication.repository.SettingServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

/**
 * REST Web Service
 *
 * @author ricardo
 */
@Controller
@Path("Settings")
public class SettingsResource {

    @Context
    private UriInfo context;

    @Autowired
    private SettingService settingService;

    /**
     * Creates a new instance of SettingsResource
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSettings() {

        ObjectMapper mapper = new ObjectMapper();
        try {

            List<Setting> Settings = settingService.findAll();

            if (Settings == null) {
                return Response.status(Response.Status.NOT_FOUND).entity("Nenhuma Settings encontrado").build();
            }

            return Response.ok(mapper.writeValueAsString(Settings), MediaType.APPLICATION_JSON).build();

        } catch (Exception e) {
            return Response.status(Response.Status.EXPECTATION_FAILED).entity("Unexpected error").build();
        }

    }
}
